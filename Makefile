all: doc

metche.8: metche.8.markdown
	pandoc --read=markdown --write=man --output=metche.8 --standalone $<

doc: metche.8

clean:

distclean: clean
	rm -f metche.8

VERSION=$(shell head -n1 Changelog | awk '{print $$1}')
TARBALL=../metche-$(VERSION).tar.gz
release: Changelog
	! [ -f $(TARBALL) ]
	git archive \
	   --prefix=metche-$(VERSION)/ \
	   --output=$(TARBALL) \
	   master

.PHONY: all doc clean distclean
